package com.lhv.nasdaq.exercise.repository;

import com.lhv.nasdaq.exercise.domain.StockInfo;
import com.lhv.nasdaq.exercise.domain.StockInfoPK;
import com.lhv.nasdaq.exercise.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link StockInfo} entity.
 */
@Repository
public interface StockInfoRepository extends JpaRepository<StockInfo, StockInfoPK> {
    List<StockInfo> findAllByIdLastModifiedDate(LocalDate givenDate);
}
