package com.lhv.nasdaq.exercise.service;


import com.lhv.nasdaq.exercise.repository.StockInfoRepository;
import com.lhv.nasdaq.exercise.service.dto.StockInfoDTO;
import com.lhv.nasdaq.exercise.service.mapper.StockInfoMapper;
import com.lhv.nasdaq.exercise.service.util.DataLoader;
import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import com.poiji.option.PoijiOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;

/**
 * Service class for managing stockInfos.
 */
@Service
@Transactional
public class StockInfoService {

    private static final String NASDAQBALTIC_URL = "https://nasdaqbaltic.com/statistics/en/shares";
    private final Logger log = LoggerFactory.getLogger(StockInfoService.class);

    private final StockInfoRepository stockInfoRepository;
    private final StockInfoMapper stockInfoMapper;
    public StockInfoService(StockInfoRepository stockInfoRepository, StockInfoMapper stockInfoMapper) {
        this.stockInfoRepository = stockInfoRepository;
        this.stockInfoMapper = stockInfoMapper;
    }

    @Transactional(readOnly = true)
    public List<StockInfoDTO> findStockInfoByDate(LocalDate localDate) {
        return stockInfoMapper.stockInfosToStockInfoDTOs(stockInfoRepository.findAllByIdLastModifiedDate(localDate));
    }

    /**
     * Fetch Data from Nasdaq website for every 30 mins.
     * <p>
     * This is scheduled to get fired every 30 mins on weekday (MON-FRI).
     */
    @Scheduled(cron = "30 * * * * MON-FRI")
    public void populateStockInfoFromNasdaq() {
        try {
            URL url = UriComponentsBuilder.fromUriString(NASDAQBALTIC_URL)
                .queryParam("download",1)
                .queryParam("date",LocalDate.now().toString())
                .build().toUri().toURL();
            DataLoader<StockInfoDTO> dataLoader = new DataLoader<>();
            List<StockInfoDTO> stockInfoDTOS = dataLoader.loadfromExcel(url.openStream(),StockInfoDTO.class);
            stockInfoRepository.saveAll(stockInfoMapper.stockInfoDTOsToStockInfos(stockInfoDTOS));
        } catch (MalformedURLException e) {
            log.error("URL does not support downloading stock info", e);
        } catch (IOException e) {
            log.error("Error reading stockinfo from excel file ", e);
        }

    }

}
