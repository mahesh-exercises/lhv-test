package com.lhv.nasdaq.exercise.service.dto;

import com.lhv.nasdaq.exercise.domain.StockInfo;
import com.poiji.annotation.ExcelCell;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import static java.util.Objects.nonNull;

/**
 * A DTO representing a Stockinfo, with only the public attributes.
 */
public class StockInfoDTO {
    @ExcelCell(0)
    private String ticker;
    private LocalDate lastModifiedDate = LocalDate.now();
    @ExcelCell(1)
    private String name;
    @ExcelCell(2)
    private String isin;
    @ExcelCell(3)
    private String currency;
    @ExcelCell(4)
    private String marketPlace;
    @ExcelCell(5)
    private String list;
    @ExcelCell(6)
    private BigDecimal averagePrice = null;
    @ExcelCell(7)
    private BigDecimal openPrice = null;
    @ExcelCell(8)
    private BigDecimal highPrice = null;
    @ExcelCell(9)
    private BigDecimal lowPrice = null;
    @ExcelCell(10)
    private BigDecimal lastClosePrice = null;
    @ExcelCell(11)
    private BigDecimal lastPrice = null;
    @ExcelCell(12)
    private BigDecimal priceChange = null;
    @ExcelCell(13)
    private BigDecimal bestBid = null;
    @ExcelCell(14)
    private BigDecimal bestAsk = null;
    @ExcelCell(15)
    private Integer trades = null;
    @ExcelCell(16)
    private Integer volume = null;
    @ExcelCell(17)
    private String industry;
    @ExcelCell(18)
    private String superSector;

    public StockInfoDTO() {
        // Empty constructor needed for Jackson.
    }

    public StockInfoDTO(StockInfo stockInfo) {
        this.ticker = stockInfo.getId().getTicker();
        this.lastModifiedDate = stockInfo.getId().getLastModifiedDate();
        this.averagePrice = nonNull(stockInfo.getAveragePrice()) ? BigDecimal.valueOf(stockInfo.getAveragePrice().doubleValue()) : null;
        this.bestAsk = nonNull(stockInfo.getBestAsk()) ? BigDecimal.valueOf(stockInfo.getBestAsk().doubleValue()) : null;
        this.bestBid = nonNull(stockInfo.getBestBid()) ? BigDecimal.valueOf(stockInfo.getBestBid().doubleValue()) : null;
        this.currency = stockInfo.getCurrency();
        this.highPrice = nonNull(stockInfo.getHighPrice()) ? BigDecimal.valueOf(stockInfo.getHighPrice().doubleValue()) : null;
        this.industry = stockInfo.getIndustry();
        this.isin = stockInfo.getIsin();
        this.lastClosePrice = nonNull(stockInfo.getLastClosePrice()) ? BigDecimal.valueOf(stockInfo.getLastClosePrice().doubleValue()) : null;
        this.lastPrice = nonNull(stockInfo.getLastPrice()) ? BigDecimal.valueOf(stockInfo.getLastPrice().doubleValue()) : null;
        this.list = stockInfo.getList();
        this.lowPrice = nonNull(stockInfo.getLowPrice()) ? BigDecimal.valueOf(stockInfo.getLowPrice().doubleValue()) : null;
        this.marketPlace = stockInfo.getMarketPlace();
        this.name = stockInfo.getName();
        this.openPrice = nonNull(stockInfo.getOpenPrice()) ? BigDecimal.valueOf(stockInfo.getOpenPrice().doubleValue()) : null;
        this.priceChange = nonNull(stockInfo.getPriceChange()) ? BigDecimal.valueOf(stockInfo.getPriceChange().doubleValue()) : null;
        this.superSector = stockInfo.getSuperSector();
        this.trades = nonNull(stockInfo.getTrades()) ? stockInfo.getTrades().intValue() : null;
        this.volume = nonNull(stockInfo.getVolume()) ? stockInfo.getVolume().intValue() : null;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMarketPlace() {
        return marketPlace;
    }

    public void setMarketPlace(String marketPlace) {
        this.marketPlace = marketPlace;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    public BigDecimal getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(BigDecimal highPrice) {
        this.highPrice = highPrice;
    }

    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    public BigDecimal getLastClosePrice() {
        return lastClosePrice;
    }

    public void setLastClosePrice(BigDecimal lastClosePrice) {
        this.lastClosePrice = lastClosePrice;
    }

    public BigDecimal getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(BigDecimal lastPrice) {
        this.lastPrice = lastPrice;
    }

    public BigDecimal getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(BigDecimal priceChange) {
        this.priceChange = priceChange;
    }

    public BigDecimal getBestBid() {
        return bestBid;
    }

    public void setBestBid(BigDecimal bestBid) {
        this.bestBid = bestBid;
    }

    public BigDecimal getBestAsk() {
        return bestAsk;
    }

    public void setBestAsk(BigDecimal bestAsk) {
        this.bestAsk = bestAsk;
    }

    public Integer getTrades() {
        return trades;
    }

    public void setTrades(Integer trades) {
        this.trades = trades;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSuperSector() {
        return superSector;
    }

    public void setSuperSector(String superSector) {
        this.superSector = superSector;
    }
}
