package com.lhv.nasdaq.exercise.service;

public class InvalidStockException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidStockException() {
        super("Incorrect Stock information.");
    }
}
