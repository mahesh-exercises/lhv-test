package com.lhv.nasdaq.exercise.service.mapper;

import com.lhv.nasdaq.exercise.domain.StockInfo;
import com.lhv.nasdaq.exercise.domain.StockInfoPK;
import com.lhv.nasdaq.exercise.service.dto.StockInfoDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link StockInfo} and its DTO called {@link StockInfoDTO}.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class StockInfoMapper {

    public List<StockInfoDTO> stockInfosToStockInfoDTOs(List<StockInfo> stockInfos) {
        return stockInfos.stream().filter(Objects::nonNull).map(this::stockInfoToStockInfoDTO).collect(Collectors.toList());
    }

    public StockInfoDTO stockInfoToStockInfoDTO(StockInfo stockInfo) {
        return new StockInfoDTO(stockInfo);
    }

    public List<StockInfo> stockInfoDTOsToStockInfos(List<StockInfoDTO> stockInfoDTOs) {
        return stockInfoDTOs.stream().filter(Objects::nonNull).map(this::stockInfoDTOToStockInfo).collect(Collectors.toList());
    }

    public StockInfo stockInfoDTOToStockInfo(StockInfoDTO stockInfoDTO) {
        if (stockInfoDTO == null) {
            return null;
        } else {
            StockInfo stockInfo = new StockInfo();
            StockInfoPK id = new StockInfoPK();
            id.setTicker(stockInfoDTO.getTicker());
            id.setLastModifiedDate(stockInfoDTO.getLastModifiedDate());
            stockInfo.setId(id);
            stockInfo.setAveragePrice(stockInfoDTO.getAveragePrice());
            stockInfo.setBestAsk(stockInfoDTO.getBestAsk());
            stockInfo.setBestBid(stockInfoDTO.getBestBid());
            stockInfo.setCurrency(stockInfoDTO.getCurrency());
            stockInfo.setHighPrice(stockInfoDTO.getHighPrice());
            stockInfo.setIndustry(stockInfoDTO.getIndustry());
            stockInfo.setIsin(stockInfoDTO.getIsin());
            stockInfo.setLastClosePrice(stockInfoDTO.getLastClosePrice());
            stockInfo.setLastPrice(stockInfoDTO.getLastPrice());
            stockInfo.setList(stockInfoDTO.getList());
            stockInfo.setLowPrice(stockInfoDTO.getLowPrice());
            stockInfo.setMarketPlace(stockInfoDTO.getMarketPlace());
            stockInfo.setName(stockInfoDTO.getName());
            stockInfo.setOpenPrice(stockInfoDTO.getOpenPrice());
            stockInfo.setPriceChange(stockInfoDTO.getPriceChange());
            stockInfo.setSuperSector(stockInfoDTO.getSuperSector());
            stockInfo.setTrades(stockInfoDTO.getTrades());
            stockInfo.setVolume(stockInfoDTO.getVolume());
            return stockInfo;
        }
    }

    public StockInfo stockInfoFromId(StockInfoPK id) {
        if (id == null) {
            return null;
        }
        StockInfo stockInfo = new StockInfo();
        stockInfo.setId(id);
        return stockInfo;
    }

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    public StockInfoDTO toDtoId(StockInfo stockInfo) {
        if (stockInfo == null) {
            return null;
        }
        StockInfoDTO stockInfoDTO = new StockInfoDTO();
        stockInfoDTO.setTicker(stockInfo.getId().getTicker());
        stockInfoDTO.setLastModifiedDate(stockInfo.getId().getLastModifiedDate());
        return stockInfoDTO;
    }


}
