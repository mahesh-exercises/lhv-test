package com.lhv.nasdaq.exercise.service.util;

import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import com.poiji.option.PoijiOptions;

import java.io.InputStream;
import java.util.List;

public class DataLoader<T> {

    public List<T> loadfromExcel(InputStream inputStream, Class<T> dtoClass) {
        PoijiOptions options = PoijiOptions.PoijiOptionsBuilder.settings().build();
        return Poiji.fromExcel(inputStream, PoijiExcelType.XLSX, dtoClass, options);
    }
}
