package com.lhv.nasdaq.exercise.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An authority (a security role) used by Spring Security.
 */
@Entity
@Table(name = "stock_info")
public class StockInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private StockInfoPK id;

    @NotNull
    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @NotNull
    @Size(max = 15)
    @Column(name = "isin", length = 15)
    private String isin;

    @NotNull
    @Size(max = 3)
    @Column(name = "currency", length = 3)
    private String currency;

    @Size(max = 3)
    @Column(name = "market_place", length = 3)
    private String marketPlace;

    @Size(max = 50)
    @Column(name = "list", length = 50)
    private String list;

    @Column(name = "average_price")
    private BigDecimal averagePrice = null;

    @Column(name = "open_price")
    private BigDecimal openPrice = null;

    @Column(name = "high_price")
    private BigDecimal highPrice = null;

    @Column(name = "low_price")
    private BigDecimal lowPrice = null;

    @Column(name = "last_close_price")
    private BigDecimal lastClosePrice = null;

    @Column(name = "last_price")
    private BigDecimal lastPrice = null;

    @Column(name = "price_change")
    private BigDecimal priceChange = null;

    @Column(name = "best_bid")
    private BigDecimal bestBid = null;

    @Column(name = "best_ask")
    private BigDecimal bestAsk = null;

    @Column(name = "trades")
    private Integer trades = null;

    @Column(name = "volume")
    private Integer volume = null;

    @Column(name = "industry", length = 50)
    private String industry;

    @Column(name = "super_sector", length = 100)
    private String superSector;

    public StockInfoPK getId() {
        return id;
    }

    public void setId(StockInfoPK id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMarketPlace() {
        return marketPlace;
    }

    public void setMarketPlace(String marketPlace) {
        this.marketPlace = marketPlace;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    public BigDecimal getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(BigDecimal highPrice) {
        this.highPrice = highPrice;
    }

    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    public BigDecimal getLastClosePrice() {
        return lastClosePrice;
    }

    public void setLastClosePrice(BigDecimal lastClosePrice) {
        this.lastClosePrice = lastClosePrice;
    }

    public BigDecimal getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(BigDecimal lastPrice) {
        this.lastPrice = lastPrice;
    }

    public BigDecimal getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(BigDecimal priceChange) {
        this.priceChange = priceChange;
    }

    public BigDecimal getBestBid() {
        return bestBid;
    }

    public void setBestBid(BigDecimal bestBid) {
        this.bestBid = bestBid;
    }

    public BigDecimal getBestAsk() {
        return bestAsk;
    }

    public void setBestAsk(BigDecimal bestAsk) {
        this.bestAsk = bestAsk;
    }

    public Integer getTrades() {
        return trades;
    }

    public void setTrades(Integer trades) {
        this.trades = trades;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSuperSector() {
        return superSector;
    }

    public void setSuperSector(String superSector) {
        this.superSector = superSector;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StockInfo)) {
            return false;
        }
        return Objects.equals(id, ((StockInfo) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "StockInfo{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", isin='" + isin + '\'' +
            ", currency='" + currency + '\'' +
            ", marketPlace='" + marketPlace + '\'' +
            ", list='" + list + '\'' +
            ", averagePrice=" + averagePrice +
            ", openPrice=" + openPrice +
            ", highPrice=" + highPrice +
            ", lowPrice=" + lowPrice +
            ", lastClosePrice=" + lastClosePrice +
            ", lastPrice=" + lastPrice +
            ", priceChange=" + priceChange +
            ", bestBid=" + bestBid +
            ", bestAsk=" + bestAsk +
            ", trades=" + trades +
            ", volume=" + volume +
            ", industry='" + industry + '\'' +
            ", superSector='" + superSector + '\'' +
            '}';
    }
}
