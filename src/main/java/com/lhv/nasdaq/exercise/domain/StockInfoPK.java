package com.lhv.nasdaq.exercise.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Embeddable
public class StockInfoPK implements Serializable {

    private static final long serialVersionUID = -5287619618362634110L;
    @NotNull
    @Size(max = 10)
    @Column(name = "ticker", length = 10)
    private String ticker;

    @NotNull
    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate = LocalDate.now();

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockInfoPK that = (StockInfoPK) o;
        return ticker.equals(that.ticker) && lastModifiedDate.equals(that.lastModifiedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticker, lastModifiedDate);
    }

    @Override
    public String toString() {
        return "StockInfoPK{" +
            "ticker='" + ticker + '\'' +
            ", lastModifiedDate=" + lastModifiedDate +
            '}';
    }
}
