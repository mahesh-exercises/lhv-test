package com.lhv.nasdaq.exercise.web.rest;

import com.lhv.nasdaq.exercise.repository.StockInfoRepository;
import com.lhv.nasdaq.exercise.service.StockInfoService;
import com.lhv.nasdaq.exercise.service.dto.StockInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing stockInfo.
 * <p>
 * This class accesses the {@link com.lhv.nasdaq.exercise.domain.StockInfo} entity.
 * We use a View Model and a DTO for 3 reasons:
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api")
public class StockInfoResource {

    private final Logger log = LoggerFactory.getLogger(StockInfoResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StockInfoService stockInfoService;

    private final StockInfoRepository stockInfoRepository;


    public StockInfoResource(StockInfoService stockInfoService, StockInfoRepository stockInfoRepository) {
        this.stockInfoService = stockInfoService;
        this.stockInfoRepository = stockInfoRepository;
    }

    /**
     * {@code GET /admin/stockInfos} : get stock information by given date
     *
     * @param givenDate the date to filter stocks.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all users.
     */
    @GetMapping("/stockInfos/{date}")
    public ResponseEntity<List<StockInfoDTO>> getAllStocks(@PathVariable LocalDate givenDate) {
        log.debug("REST request to get all StockInfos");
        if (givenDate == null) givenDate = LocalDate.now();
        return ResponseUtil.wrapOrNotFound(Optional.of(stockInfoService.findStockInfoByDate(givenDate)));
    }


}
