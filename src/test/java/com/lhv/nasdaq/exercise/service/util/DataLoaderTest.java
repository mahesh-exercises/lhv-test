package com.lhv.nasdaq.exercise.service.util;

import com.lhv.nasdaq.exercise.service.dto.StockInfoDTO;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DataLoaderTest {

    @Test
    void loadFromExcelFile() {
        InputStream in = this.getClass().getClassLoader()
            .getResourceAsStream("nasdaq_shares_sample.xlsx");
        DataLoader<StockInfoDTO> dataLoader = new DataLoader<>();
        List<StockInfoDTO> stockInfoDTOS = dataLoader.loadfromExcel(in, StockInfoDTO.class);
        assertThat(stockInfoDTOS).isNotEmpty().size().isEqualTo(6);
    }
}
