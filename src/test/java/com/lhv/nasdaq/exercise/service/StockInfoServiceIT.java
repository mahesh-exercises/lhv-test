package com.lhv.nasdaq.exercise.service;

import com.lhv.nasdaq.exercise.IntegrationTest;
import com.lhv.nasdaq.exercise.domain.StockInfo;
import com.lhv.nasdaq.exercise.domain.StockInfoPK;
import com.lhv.nasdaq.exercise.repository.StockInfoRepository;
import com.lhv.nasdaq.exercise.service.dto.StockInfoDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link StockInfoService}.
 */
@IntegrationTest
@Transactional
class StockInfoServiceIT {

    private static final String DEFAULT_TICKER = "lhv";
    private static final LocalDate DEFAULT_DATE = LocalDate.now();
    private static final String DEFAULT_CURRENCY = "EUR";
    private static final String DEFAULT_MARKETPLACE = "XYZ";
    private static final String DEFAULT_ISIN = "LHV123456";

    @Autowired
    private StockInfoRepository stockInfoRepository;

    @Autowired
    private StockInfoService stockInfoService;

    private StockInfo stockInfo;

    @BeforeEach
    public void init() {
        stockInfo = new StockInfo();
        StockInfoPK id = new StockInfoPK();
        id.setTicker(DEFAULT_TICKER);
        id.setLastModifiedDate(DEFAULT_DATE);
        stockInfo.setId(id);
        stockInfo.setSuperSector(RandomStringUtils.random(60));
        stockInfo.setList(RandomStringUtils.random(20));
        stockInfo.setName(RandomStringUtils.random(60));
        stockInfo.setIndustry(RandomStringUtils.random(40));
        stockInfo.setCurrency(DEFAULT_CURRENCY);
        stockInfo.setMarketPlace(DEFAULT_MARKETPLACE);
        stockInfo.setIsin(DEFAULT_ISIN);
    }

    @Test
    @Transactional
    void assertThatStockCanBeFetchByDate() {
        stockInfoRepository.saveAndFlush(stockInfo);
        List<StockInfoDTO> stocks = stockInfoService.findStockInfoByDate(stockInfo.getId().getLastModifiedDate());
        assertThat(stocks).isNotEmpty();
    }

}
