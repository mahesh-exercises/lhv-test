package com.lhv.nasdaq.exercise.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.lhv.nasdaq.exercise.domain.StockInfo;
import com.lhv.nasdaq.exercise.domain.StockInfoPK;
import com.lhv.nasdaq.exercise.domain.User;
import com.lhv.nasdaq.exercise.service.dto.StockInfoDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.lhv.nasdaq.exercise.web.rest.TestUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for {@link StockInfoMapper}.
 */
class StockInfoMapperTest {

    private static final String DEFAULT_TICKER = "lhv";
    private static final LocalDate DEFAULT_DATE = LocalDate.now();
    private static final String DEFAULT_CURRENCY = "EUR";
    private static final String DEFAULT_MARKETPLACE = "XYZ";
    private static final String DEFAULT_ISIN = "LHV123456";
    private static final BigDecimal DEFAULT_AVG_PRICE = new BigDecimal(10.12345);

    private StockInfoMapper stockInfoMapper;
    private StockInfo stockInfo;
    private StockInfoDTO stockInfoDTO;

    @BeforeEach
    public void init() {
        stockInfoMapper = new StockInfoMapper();
        stockInfo = new StockInfo();
        StockInfoPK id = new StockInfoPK();
        id.setTicker(DEFAULT_TICKER);
        id.setLastModifiedDate(DEFAULT_DATE);
        stockInfo.setId(id);
        stockInfo.setSuperSector(RandomStringUtils.random(60));
        stockInfo.setList(RandomStringUtils.random(20));
        stockInfo.setName(RandomStringUtils.random(60));
        stockInfo.setIndustry(RandomStringUtils.random(40));
        stockInfo.setCurrency(DEFAULT_CURRENCY);
        stockInfo.setMarketPlace(DEFAULT_MARKETPLACE);
        stockInfo.setAveragePrice(DEFAULT_AVG_PRICE);
        stockInfo.setIsin(DEFAULT_ISIN);

        stockInfoDTO = new StockInfoDTO(stockInfo);
    }

    @Test
    void stockInfosToStockInfoDTOsShouldMapOnlyNonNullStocks() {
        List<StockInfo> stockInfos = new ArrayList<>();
        stockInfos.add(stockInfo);
        stockInfos.add(null);
        List<StockInfoDTO> stockInfoDTOS = stockInfoMapper.stockInfosToStockInfoDTOs(stockInfos);
        assertThat(stockInfoDTOS).isNotEmpty().size().isEqualTo(1);
    }

    @Test
    void stockInfoDTOsToStockInfosShouldMapOnlyNonNullStocks() {
        List<StockInfoDTO> stockInfoDTOS = new ArrayList<>();
        stockInfoDTOS.add(stockInfoDTO);
        stockInfoDTOS.add(null);

        List<StockInfo> stockInfos = stockInfoMapper.stockInfoDTOsToStockInfos(stockInfoDTOS);

        assertThat(stockInfos).isNotEmpty().size().isEqualTo(1);
    }

    @Test
    void stockInfoDTOToStockInfoMapWithNullStockInfoShouldReturnNull() {
        assertThat(stockInfoMapper.stockInfoDTOToStockInfo(null)).isNull();
    }

    @Test
    void testStockInfoDTOtoStockInfo() {
        StockInfoDTO stockInfoDTO = new StockInfoDTO();
        stockInfoDTO.setTicker(DEFAULT_TICKER);
        stockInfoDTO.setLastModifiedDate(DEFAULT_DATE);
        stockInfoDTO.setIsin(DEFAULT_ISIN);
        stockInfoDTO.setCurrency(DEFAULT_CURRENCY);
        stockInfoDTO.setMarketPlace(DEFAULT_MARKETPLACE);
        stockInfoDTO.setAveragePrice(DEFAULT_AVG_PRICE);

        StockInfo stockInfo = stockInfoMapper.stockInfoDTOToStockInfo(stockInfoDTO);
        StockInfoPK stockInfoPK = new StockInfoPK();
        stockInfoPK.setTicker(DEFAULT_TICKER);
        stockInfoPK.setLastModifiedDate(DEFAULT_DATE);
        assertThat(stockInfo.getId()).isEqualTo(stockInfoPK);
        assertThat(stockInfo.getIsin()).isEqualTo(DEFAULT_ISIN);
        assertThat(stockInfo.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(stockInfo.getMarketPlace()).isEqualTo(DEFAULT_MARKETPLACE);
        assertThat(stockInfo.getAveragePrice()).isEqualTo(DEFAULT_AVG_PRICE);
    }

    @Test
    void testStockInfoToStockInfoDTO() {
        StockInfoDTO stockInfoDTO = stockInfoMapper.stockInfoToStockInfoDTO(stockInfo);
        assertThat(stockInfoDTO.getTicker()).isEqualTo(DEFAULT_TICKER);
        assertThat(stockInfoDTO.getLastModifiedDate()).isEqualTo(DEFAULT_DATE);
        assertThat(stockInfoDTO.getAveragePrice().doubleValue()).isEqualTo(DEFAULT_AVG_PRICE.doubleValue());
        assertThat(stockInfoDTO.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(stockInfoDTO.getIsin()).isEqualTo(DEFAULT_ISIN);
        assertThat(stockInfoDTO.getMarketPlace()).isEqualTo(DEFAULT_MARKETPLACE);
        assertThat(stockInfoDTO.toString()).isNotNull();
    }

    @Test
    void testUserEquals() throws Exception {
        TestUtil.equalsVerifier(StockInfo.class);
        StockInfo otherStockInfo = new StockInfo();
        StockInfoPK id = new StockInfoPK();
        id.setTicker(DEFAULT_TICKER);
        id.setLastModifiedDate(DEFAULT_DATE);
        otherStockInfo.setId(id);
        otherStockInfo.setSuperSector(RandomStringUtils.random(60));
        otherStockInfo.setList(RandomStringUtils.random(20));
        otherStockInfo.setName(RandomStringUtils.random(60));
        otherStockInfo.setIndustry(RandomStringUtils.random(40));
        otherStockInfo.setCurrency(DEFAULT_CURRENCY);
        otherStockInfo.setMarketPlace(DEFAULT_MARKETPLACE);
        otherStockInfo.setIsin(DEFAULT_ISIN);
        assertThat(stockInfo).isEqualTo(otherStockInfo);
        otherStockInfo.getId().setLastModifiedDate(LocalDate.now().minusDays(2));
        assertThat(stockInfo).isNotEqualTo(otherStockInfo);
    }
}
