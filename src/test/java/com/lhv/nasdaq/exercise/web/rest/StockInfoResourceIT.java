package com.lhv.nasdaq.exercise.web.rest;

import com.lhv.nasdaq.exercise.IntegrationTest;
import com.lhv.nasdaq.exercise.domain.StockInfo;
import com.lhv.nasdaq.exercise.domain.StockInfoPK;
import com.lhv.nasdaq.exercise.domain.User;
import com.lhv.nasdaq.exercise.repository.StockInfoRepository;
import com.lhv.nasdaq.exercise.service.mapper.StockInfoMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StockInfoResource} REST controller.
 */
@AutoConfigureMockMvc
@WithMockUser
@IntegrationTest
class StockInfoResourceIT {

    private static final String DEFAULT_TICKER = "lhv";
    private static final LocalDate DEFAULT_DATE = LocalDate.now();
    private static final String DEFAULT_CURRENCY = "EUR";
    private static final String DEFAULT_MARKETPLACE = "XYZ";
    private static final String DEFAULT_ISIN = "LHV123456";

    @Autowired
    private StockInfoRepository stockInfoRepository;

    @Autowired
    private StockInfoMapper stockInfoMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStockInfoMockMvc;

    private StockInfo stockInfo;

    /**
     * Setups the database with one user.
     */
    public static StockInfo initTestStockInfo(StockInfoRepository stockInfoRepository, EntityManager em) {
        StockInfo stockInfo = new StockInfo();
        StockInfoPK id = new StockInfoPK();
        id.setTicker(DEFAULT_TICKER);
        id.setLastModifiedDate(DEFAULT_DATE);
        stockInfo.setId(id);
        stockInfo.setSuperSector(RandomStringUtils.random(60));
        stockInfo.setList(RandomStringUtils.random(20));
        stockInfo.setName(RandomStringUtils.random(60));
        stockInfo.setIndustry(RandomStringUtils.random(40));
        stockInfo.setCurrency(DEFAULT_CURRENCY);
        stockInfo.setMarketPlace(DEFAULT_MARKETPLACE);
        stockInfo.setIsin(DEFAULT_ISIN);
        return stockInfo;
    }

    @BeforeEach
    public void initTest() {
        stockInfo = initTestStockInfo(stockInfoRepository, em);
    }

    @Test
    @Transactional
    void getUser() throws Exception {
        // Initialize the database
        stockInfoRepository.saveAndFlush(stockInfo);

        // Get the user
        restStockInfoMockMvc
            .perform(get("/api/stockInfos/{date}", stockInfo.getId().getLastModifiedDate()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.ticker").value(stockInfo.getId().getTicker()))
            .andExpect(jsonPath("$.name").value(stockInfo.getName()))
            .andExpect(jsonPath("$.isin").value(DEFAULT_ISIN));
    }

    @Test
    @Transactional
    void getNonExistingUser() throws Exception {
        restStockInfoMockMvc.perform(get("/api/admin/users/unknown")).andExpect(status().isNotFound());
    }
}
